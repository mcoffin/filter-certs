use std::{
    io::{
        self,
        BufReader,
        BufRead,
        ErrorKind,
    },
};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Status {
    Waiting,
    InCertificate,
}

impl Status {
    pub const fn new() -> Status {
        Status::Waiting
    }

    pub fn handle_line<S: AsRef<str>>(self, line: S) -> Status {
        let line = line.as_ref();
        match self {
            Status::Waiting if line == "-----BEGIN CERTIFICATE-----" => {
                println!("{}", line);
                Status::InCertificate
            },
            Status::InCertificate => {
                println!("{}", line);
                if line == "-----END CERTIFICATE-----" {
                    Status::Waiting
                } else {
                    Status::InCertificate
                }
            },
            _ => self,
        }
    }

    pub fn finish(&self) -> io::Result<()> {
        if self != &Status::Waiting {
            Err(io::Error::new(ErrorKind::UnexpectedEof, "Unexpected EOF in middle of certificate"))
        } else {
            Ok(())
        }
    }
}

trait ResultExt<T, E>: Sized {
    fn and_then_tup<F, Ret>(self, f: F) -> Result<(T, Ret), E>
    where
        Ret: Sized,
        F: FnOnce(&T) -> Result<Ret, E>;

    #[inline(always)]
    fn and_tup<Ret>(self, v: Result<Ret, E>) -> Result<(T, Ret), E> {
        self.and_then_tup(move |_| v)
    }
}

impl<T, E> ResultExt<T, E> for Result<T, E> where
    T: Sized,
    E: Sized,
{
    #[inline(always)]
    fn and_then_tup<F, Ret>(self, f: F) -> Result<(T, Ret), E>
    where
        Ret: Sized,
        F: FnOnce(&T) -> Result<Ret, E>,
    {
        self.and_then(move |v| f(&v).map(move |vv| (v, vv)))
    }
}

fn real_main() -> io::Result<()> {
    let stdin = BufReader::new(io::stdin());
    let status = stdin.lines().fold(Ok(Status::new()), |status, line| status.and_tup(line).map(|(status, line)| {
        status.handle_line(&line)
    }))?;
    status.finish()
}

fn main() {
    real_main()
        .expect("Failed while filtering certificates");
}
